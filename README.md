# SailorD - a SystemD Manager for Sailfish OS

SailorD shows and conreols interesting systemd units, and can act as a simple
monitor for them.

Notifications are triggered if one of them goes into 'failed' state. The app
must be running for monitoring or notifications to happen.

Features:

 - list Units of type: `service`, `mount`, `timer`, `path`
 - show status in listing and additional properties on click
 - start/stop/enable/disable units (experimental)
 - switch between Session and System DBUS (experimental)
 - show alerts/notifications when an unit goes into `"failed"` state
 - view the unit file and send it to another app

----

**Some words of warning**:   

If you enable "Unit Control", i.e. the feature to start/stop/... unit, you
should have  a thorough understanding of what the different states and service
types mean and how to interpret them.  

On first view, you might see many services in some kind of "not running" state.
For most of them this is *okay* and *normal*. **Do not try** to manually start
them. (Things like oneshot or dbus-activated services are not running until
triggered.)  
Sometimes even a *failed* state is okay and normal -  e.g.  apparently
dismissed alarms are implemented as a service that fails (and restarts) until
the alarm is aquitted.

Conversely, stopping or disabling running services may also have unintended
consequences, for obvious and not-so-obvious reasons. This may range from
application not launching (e.g. because you stopped a booster service) to your
phone not booting any more.

The author(s) of this app can not and will not support such issues caused by
usage of SailorD.

*Caveat emptor*.

----

Internally, systemd's D-Bus interfaces are used, so any information listed,
and actions taken are subject to that interface's permission setup.

See the systemd [Documentation](https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html)
for details if interested.

----

License: Apache-2.0
