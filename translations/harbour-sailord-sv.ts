<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation>Källkod:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Erkännanden</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Översättningar: %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>failed</source>
        <translation>misslyckades</translation>
    </message>
    <message>
        <source>units</source>
        <translation>enheter</translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation>Enhetsegenskaper</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation>Tjänstegenskaper</translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation>Timeregenskaper</translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation>Monteringsegenskaper</translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation>Sökvägsegenskaper</translation>
    </message>
    <message>
        <source>Unit name copied to clipboard</source>
        <translation type="vanished">Enhetsnamn kopierat till urklipp</translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation>Långtryck igen för att kopiera detaljerad info</translation>
    </message>
    <message>
        <source>Unit details copied to clipboard</source>
        <translation type="vanished">Enhetsdetaljer kopierat till urklipp</translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation>Andra namn</translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation>Beroenden</translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation>Enhetsfil</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation>Kopiera sökväg</translation>
    </message>
    <message>
        <source>Unit file path copied to clipboard</source>
        <translation type="vanished">Enhetsfilens sökväg kopierad till urklipp</translation>
    </message>
    <message>
        <source>Show Content</source>
        <translation>Visa innehåll</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation>%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Enhetsdetaljer</translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Enhetsnamn</translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Enhetsfilens sökväg</translation>
    </message>
    <message>
        <source>Socket Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation>Visa aviseringar</translation>
    </message>
    <message>
        <source>Ignored Units:</source>
        <translation>Undantagna enheter:</translation>
    </message>
    <message>
        <source>Clear permanent ignore list</source>
        <translation>Rensa permanent undantagslista</translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation>Listan rensad.</translation>
    </message>
    <message>
        <source>Clear session ignore list</source>
        <translation>Rensa listan över sessionsundantag</translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation>Fasta aviseringar</translation>
    </message>
    <message>
        <source>If enabled, the app will update a single notification (as opposed to sending a new one each time).</source>
        <translation>Vid aktivering uppdaterar appen en avisering (i motsats till att skicka en ny varje gång).</translation>
    </message>
    <message>
        <source>These units are permanently ignored:</source>
        <translation>Dessa enheter är permanent undantagna:</translation>
    </message>
    <message>
        <source>These units are ignored in this session:</source>
        <translation>Dessa enheter är undantagna denna session:</translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation>Avancerat:</translation>
    </message>
    <message>
        <source>Hide Firmware mounts</source>
        <translation>Dölj monterad firmware</translation>
    </message>
    <message>
        <source>If enabled, the app will not list firmware (Android Base, Hybris) mounts.</source>
        <translation>Vid aktivering listar appen inte monterad firmware (Android Base, Hybris).</translation>
    </message>
    <message>
        <source>Hide uninteresting units</source>
        <translation>Dölj ointressanta enheter</translation>
    </message>
    <message>
        <source>If enabled, the app will not list units that are plugged, tentative, or similar.</source>
        <translation>Vid aktivering listas inte enheter som är anslutna, preliminära eller liknande.</translation>
    </message>
    <message>
        <source>Control Units</source>
        <translation>Kontrollenheter</translation>
    </message>
    <message>
        <source>Bus Switching</source>
        <translation>Bussväxling</translation>
    </message>
    <message>
        <source>If enabled, you will be able to toggle access to the System Bus.</source>
        <translation>Vid aktivering kan du växla åtkomst till systembussen.</translation>
    </message>
    <message>
        <source>If enabled, the app will send notifications about failed units.&lt;br /&gt; Use the slider below to specify how often to check.</source>
        <translation>Vid aktivering skickar appen meddelanden om misslyckade enheter.&lt;br /&gt; Använd skjutreglaget nedan för att ange hur ofta kontroll skall ske.</translation>
    </message>
    <message>
        <source>Offer Ignore for all units</source>
        <translation>Erbjud undantag för alla enheter</translation>
    </message>
    <message>
        <source>If enabled, the app will offer the ignore opton for any unit (not just failed ones).</source>
        <translation>Vid aktivering kommer appen att erbjuda undantag för alla enheter (inte bara misslyckade).</translation>
    </message>
    <message>
        <source>💥 ☠ WARNING! DANGER! DRAGONS! ☠ 💥</source>
        <translation>💥 ☠ VARNING! FARA! DRAKAR! ☠ 💥</translation>
    </message>
    <message>
        <source>This app does little to no checking whether your actions actually make sense.&lt;br /&gt;Make sure you have a thorough understanding of how systemd units and their status work before enabling the options below.&lt;br /&gt;If you break something, you get to keep ALL THE PIECES.</source>
        <translation>Den här appen har lite eller ingen kontroll över om dina handlingar faktiskt är vettiga.&lt;br /&gt;Tillse att du har grundlig förståelse för hur systemd-enheter och deras status fungerar innan du aktiverar alternativen nedan.&lt;br /&gt;Om du gör sönder något får du behålla ALLA BITAR.</translation>
    </message>
    <message>
        <source>If enabled, you will be able to start, stop, enable, disable units.&lt;br /&gt;
                    Even if enabled, standard systemd/dbus/polkit permissions apply. This means some of the operations may not succeed, or ask for authorization.&lt;br /&gt;
</source>
        <translation>Vid aktivering kommer du att kunna starta, stoppa, aktivera eller inaktivera enheter.&lt;br /&gt;
                    Även om det är aktiverat gäller standardbehörigheter för systemd/dbus/polkit. Det innebär att vissa av åtgärderna kanske inte lyckas eller ber om auktorisering.&lt;br /&gt;
</translation>
    </message>
    <message>
        <source>Check every</source>
        <translation>Kontrollera varje</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>⚠ Danger Zone 🛦🛦🛦</source>
        <translation>⚠ Riskområde 🛦🛦🛦</translation>
    </message>
    <message>
        <source>You can clear these lists using the PullUp Menu</source>
        <translation type="vanished">Du kan rensa dessa listor med dra-upp-menyn</translation>
    </message>
    <message>
        <source>No ignored units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Success Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If disabled, the app will only show popups about operation failures. Otherwise, succeess is reported as popup message.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation>Undanta denna session</translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation>Undanta permanent</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Starta om</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Aktivera</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation>Inaktivera</translation>
    </message>
    <message>
        <source>Mount</source>
        <translation>Montera</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation>Avmontera</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Starta</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Stoppa</translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation>Inga åtgärder tillgängliga.</translation>
    </message>
    <message>
        <source>Remount</source>
        <translation>Återmontera</translation>
    </message>
    <message>
        <source>next</source>
        <translation>nästa</translation>
    </message>
    <message>
        <source>triggered by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>dbus-activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>oneshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>spawned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>spawns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation>Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation>Inaktiverar%1</translation>
    </message>
    <message>
        <source>Starting %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation>Stoppar %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation>Startar om %1</translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message numerus="yes">
        <source>%n unit(s) ignored</source>
        <translation>
            <numerusform>%n enhet undantagen</numerusform>
            <numerusform>%n enheter undantagna</numerusform>
        </translation>
    </message>
    <message>
        <source>Failed units detected.</source>
        <translation>Misslyckade enheter identifierade.</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <source>Timers</source>
        <comment>Page title unit type</comment>
        <translation>Timers</translation>
    </message>
    <message>
        <source>Paths</source>
        <comment>Page title unit type</comment>
        <translation>Sökvägar</translation>
    </message>
    <message>
        <source>Mounts</source>
        <comment>Page title unit type</comment>
        <translation>Monteringar</translation>
    </message>
    <message>
        <source>Services</source>
        <comment>Page title unit type</comment>
        <translation>Tjänster</translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation>Växla till %1</translation>
    </message>
    <message>
        <source>System Bus</source>
        <translation>Systembuss</translation>
    </message>
    <message>
        <source>Session Bus</source>
        <translation>Sessionsbuss</translation>
    </message>
    <message>
        <source>Sockets</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unit Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search by name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Targets</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Copy file path to Clipboard</source>
        <translation type="vanished">Kopiera filsökväg till urklipp</translation>
    </message>
    <message>
        <source>Copy contents to Clipboard</source>
        <translation type="vanished">Kopiera innehåll till urklipp</translation>
    </message>
    <message>
        <source>Share as Unit…</source>
        <translation type="vanished">Dela som enhet…</translation>
    </message>
    <message>
        <source>Share as Text…</source>
        <translation>Dela som text…</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation>Öppna…</translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation>Kopiera %1 till urklipp</translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation>filsökväg</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation>%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation>innehåll</translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation>Dela %1…</translation>
    </message>
    <message>
        <source>Unit File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation type="unfinished">Enhetsfil</translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation>%1 slutfört.</translation>
    </message>
    <message>
        <source>Success:</source>
        <translation>Slutfört:</translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation>%1 misslyckades.</translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation>Misslyckat:</translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation>Förinställ %1</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Aktivera</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation>Aktivera %1</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="obsolete">Inaktivera</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation>Inaktivera %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Starta</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation>Starta %1</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Stoppa</translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation>Stoppa %1</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Starta om</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation>Starta om %1</translation>
    </message>
    <message>
        <source>%2 for unit %3</source>
        <comment>successful message, e.g. &apos;start for unit foo&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 %2 for unit %3</source>
        <comment>failure message, e.g. &apos;starting failed for unit foo&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DBus service %1 ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
