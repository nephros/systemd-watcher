<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>units</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Socket Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, the app will send notifications about failed units.&lt;br /&gt; Use the slider below to specify how often to check.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, the app will update a single notification (as opposed to sending a new one each time).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offer Ignore for all units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, the app will offer the ignore opton for any unit (not just failed ones).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignored Units:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No ignored units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear permanent ignore list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>These units are permanently ignored:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear session ignore list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>These units are ignored in this session:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Firmware mounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, the app will not list firmware (Android Base, Hybris) mounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide uninteresting units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, the app will not list units that are plugged, tentative, or similar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>⚠ Danger Zone 🛦🛦🛦</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>💥 ☠ WARNING! DANGER! DRAGONS! ☠ 💥</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This app does little to no checking whether your actions actually make sense.&lt;br /&gt;Make sure you have a thorough understanding of how systemd units and their status work before enabling the options below.&lt;br /&gt;If you break something, you get to keep ALL THE PIECES.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bus Switching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, you will be able to toggle access to the System Bus.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Control Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, you will be able to start, stop, enable, disable units.&lt;br /&gt;
                    Even if enabled, standard systemd/dbus/polkit permissions apply. This means some of the operations may not succeed, or ask for authorization.&lt;br /&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Success Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If disabled, the app will only show popups about operation failures. Otherwise, succeess is reported as popup message.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>triggered by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>dbus-activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>oneshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>spawned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>spawns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message numerus="yes">
        <source>%n unit(s) ignored</source>
        <translation>
            <numerusform>one unit ignored</numerusform>
            <numerusform>%n units ignored</numerusform>
        </translation>
    </message>
    <message>
        <source>Timers</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paths</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sockets</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mounts</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed units detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unit Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Services</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Bus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Session Bus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search by name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Targets</source>
        <comment>Page title unit type</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Share as Text…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unit File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Success:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%2 for unit %3</source>
        <comment>successful message, e.g. &apos;start for unit foo&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 %2 for unit %3</source>
        <comment>failure message, e.g. &apos;starting failed for unit foo&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DBus service %1 ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
