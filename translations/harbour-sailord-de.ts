<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>What&apos;s %1</source>
        <translation type="vanished">Was soll %1 sein</translation>
    </message>
    <message>
        <source>What&apos;s %1?</source>
        <translation type="vanished">Was soll %1? sein</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Übersetzung: %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>failed</source>
        <translation>fehlgeschlagen</translation>
    </message>
    <message>
        <source>units</source>
        <translation>Units</translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation>Elementmerkmale</translation>
    </message>
    <message>
        <source>Names</source>
        <translation type="vanished">Namen</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation>Dienstmerkmale</translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation>Zeitgebermerkmale</translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation>Einhängepunktperkmale</translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation>Pfadmerkmale</translation>
    </message>
    <message>
        <source>Unit information copied to clipboard</source>
        <translation type="vanished">Elementinformationen in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <source>Unit name copied to clipboard</source>
        <translation type="vanished">Elementname in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation>Lange gedrückt halten, um Details zu kopieren</translation>
    </message>
    <message>
        <source>Unit details copied to clipboard</source>
        <translation type="vanished">Elementinformationen in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation>Weitere Namen</translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation>Abhängigkeiten</translation>
    </message>
    <message>
        <source>Show Unit File</source>
        <translation type="vanished">Elementdatei anzeigen</translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation>Elementdatei</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation>Pfad Kopieren</translation>
    </message>
    <message>
        <source>Unit file path copied to clipboard</source>
        <translation type="vanished">Elementdateipfad kopiert</translation>
    </message>
    <message>
        <source>Show Content</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation>%1 nun in der Zwischenablage</translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Elementdetails</translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Elementname</translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation>Elementdateipfad</translation>
    </message>
    <message>
        <source>Socket Properties</source>
        <translation>Socketmerkmale</translation>
    </message>
</context>
<context>
    <name>ServiceDetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation type="vanished">Unit-Merkmale</translation>
    </message>
    <message>
        <source>Names</source>
        <translation type="vanished">Namen</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation type="vanished">Dienstmerkmale</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>App</translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation>Benachrichtigungen anzeigen</translation>
    </message>
    <message>
        <source>If enabled, the app will send notifications (mainly about failed units).</source>
        <translation type="vanished">Benachrichtiungen (v.A. zu vestorbenen Units) anzeigen.</translation>
    </message>
    <message>
        <source>How often to check for (and notify about) failed units.</source>
        <translation type="vanished">Wie oft geprüft (und benachrichtige) werden soll.</translation>
    </message>
    <message>
        <source>Remember ignored Units</source>
        <translation type="vanished">Verborgene Units merken</translation>
    </message>
    <message>
        <source>If enabled, the app will remember which units you set to ignore.</source>
        <translation type="vanished">Units, die verborgen wurden, werden gespeichert</translation>
    </message>
    <message>
        <source>Ignored Units:</source>
        <translation>Verborgene Units:</translation>
    </message>
    <message>
        <source>Clear ignore list</source>
        <translation type="vanished">Verbergungsliste leeren</translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation>Liste geleert.</translation>
    </message>
    <message>
        <source>Check interval</source>
        <translation type="vanished">Intervall</translation>
    </message>
    <message>
        <source>%1 seconds</source>
        <translation type="vanished">%1 Sekunden</translation>
    </message>
    <message>
        <source>Clear permanent ignore list</source>
        <translation>Permanentverbergungsliste leeren</translation>
    </message>
    <message>
        <source>Clear session ignore list</source>
        <translation>Sitzungsverbergungsliste leeren</translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation>Benachrichtigungen halten</translation>
    </message>
    <message>
        <source>Interval in seconds</source>
        <translation type="vanished">Intervall in Sekunden</translation>
    </message>
    <message>
        <source>These units are permanently ignored:</source>
        <translation>Diese Einheiten werden permanent verborgen</translation>
    </message>
    <message>
        <source>These units are ignored in this session:</source>
        <translation>Diese Einheiten werden nur in dieser Sitzung verborgen</translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation>Erweitert:</translation>
    </message>
    <message>
        <source>Hide Firmware mounts</source>
        <translation>Firmwareeinhängepunkte verbergen</translation>
    </message>
    <message>
        <source>If enabled, the app will not list firmware (Android Base, Hybris) mounts.</source>
        <translation>Wenn eingeschalten werden Einhängepunkte der Firmware (Android Base, Hybris) nicht angezeigt.</translation>
    </message>
    <message>
        <source>Hide uninteresting units</source>
        <translation>Uninteressante Einheiten verbergen</translation>
    </message>
    <message>
        <source>If enabled, the app will not list units that are plugged, tentative, or similar.</source>
        <translation>Wenn eingeschalten zeigt die app Einheiten, die im Zustand &quot;plugged&quot;, &quot;tentative&quot; o.ä. sind, nicht an.</translation>
    </message>
    <message>
        <source>⚠ Danger Zone: 🛦🛦🛦</source>
        <translation type="vanished">⚠ Danger Zone: 🛦🛦🛦</translation>
    </message>
    <message>
        <source>💥 ☠ WARNING! DANGER! DRAGONS! ☠ 💥&lt;br /&gt;
The app does little checking whether your action makes sense. If you break something, you get to keep ALL THE PIECES.&lt;br /&gt;
</source>
        <translation type="vanished">💥 ☠ ACHTUNG! VORSICHT! WAHRSCHAU! ☠ 💥&lt;br /&gt;
Die app prüft nicht, bob was du versuchst Blödsinn ist. Wenn was zerbricht, kannst du sämtliche Teile selbst behalten.&lt;br /&gt;
</translation>
    </message>
    <message>
        <source>Control Units</source>
        <translation>Einheiten bearbeiten</translation>
    </message>
    <message>
        <source>If enabled, you will be able to start, stop, enable, disable units.&lt;br /&gt;
Even if enabled, standard systemd/dbus/polkit permissions apply. This means some of the operations may not succeed.&lt;br /&gt;
</source>
        <translation type="vanished">Wenn eingeschalten kannst du Einheiten starten, stoppen, aktivieren und deaktivieren&lt;br /&gt;
Auch wenn dies eingeschalten ist, bleiben die Standardberechtigungen von systemd/dbus/polkit. Manche aktionen werden also trotzdem fehlschlagen.&lt;br /&gt;
</translation>
    </message>
    <message>
        <source>If enabled, the app will update a single notification (as opposed to sending a new one each time).</source>
        <translation>Wenn eingeschalten wird die selbe Benachrichtigung immer wieder aktualisiert (und nicht jedesmal eine neue geschickt).</translation>
    </message>
    <message>
        <source>Bus Switching</source>
        <translation>Buswechsel</translation>
    </message>
    <message>
        <source>If enabled, you will be able to toggle access to the System Bus.</source>
        <translation>Wenn eingeschalten kannst du swischen Sitzungs- und Systembus hin- und her wechseln.</translation>
    </message>
    <message>
        <source>If enabled, the app will send notifications about failed units.&lt;br /&gt; Use the slider below to specify how often to check.</source>
        <translation>Benachrichtiungen über vestorbene Einheiten anzeigen.&lt;br /&gt;Mit dem Regler stellst du ein, wie often darauf geprüft wird.</translation>
    </message>
    <message>
        <source>Offer Ignore for all units</source>
        <translation>&apos;Verbergen&apos; für alle Einheiten azeigen</translation>
    </message>
    <message>
        <source>If enabled, the app will offer the ignore opton for any unit (not just failed ones).</source>
        <translation>Wenn eingeschalten wird das Verbergen-Menü für alle (nicht nur gestorbene) Einheiten angeboten.</translation>
    </message>
    <message>
        <source>You can clear these lists using the PuashUp Menu</source>
        <translation type="vanished">Du kannst diese Listen mit dem unteren Zieh-Menü leeren</translation>
    </message>
    <message>
        <source>💥 ☠ WARNING! DANGER! DRAGONS! ☠ 💥</source>
        <translation>💥 ☠ ACHTUNG! VORSICHT! WAHRSCHAU! ☠ 💥</translation>
    </message>
    <message>
        <source>If enabled, you will be able to start, stop, enable, disable units.&lt;br /&gt;
                    Even if enabled, standard systemd/dbus/polkit permissions apply. This means some of the operations may not succeed, or ask for authorization.&lt;br /&gt;
</source>
        <translation>Wenn eingeschalten kannst du Einheiten starten, stoppen, aktivieren und deaktivieren&lt;br /&gt;
Auch wenn dies eingeschalten ist, bleiben die Standardberechtigungen von systemd/dbus/polkit. Manche aktionen werden also trotzdem fehlschlagen.&lt;br /&gt;</translation>
    </message>
    <message>
        <source>This app does little to no checking whether your actions actually make sense.&lt;br /&gt;Make sure you have a thorough understanding of how systemd units and their status work before enabling the options below.&lt;br /&gt;If you break something, you get to keep ALL THE PIECES.</source>
        <translation>Die app prüft nicht, bob was du versuchst Blödsinn ist. Du solltest systemd-Einheiten und ihre Status gut verstanden haben bevor du untige Optionen einschaltest.&lt;br /&gt;Wenn was zerbricht, kannst du sämtliche Teile selbst behalten.&lt;br /&gt; </translation>
    </message>
    <message>
        <source>Check every</source>
        <translation>Prüfe alle…</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>⚠ Danger Zone 🛦🛦🛦</source>
        <translation>Danger Zone</translation>
    </message>
    <message>
        <source>You can clear these lists using the PullUp Menu</source>
        <translation type="vanished">Du kannst diese Listen mit dem unteren Zieh-Menü leeren</translation>
    </message>
    <message>
        <source>No ignored units</source>
        <translation>Keine verborgenen Einheiten in der Liste</translation>
    </message>
    <message>
        <source>Show Success Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If disabled, the app will only show popups about operation failures. Otherwise, succeess is reported as popup message.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Unit</name>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Verbergen</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Neu Starten</translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation>In dieser Sitzung verbergen</translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation>Immer verbergen</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Neu Starten</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <source>Mount</source>
        <translation>Einhängen</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation>Abhängen</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Starten</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation>Keine Aktionen vorhanden.</translation>
    </message>
    <message>
        <source>Remount</source>
        <translation>Ab- und neu Einhängen</translation>
    </message>
    <message>
        <source>next</source>
        <translation>nächster</translation>
    </message>
    <message>
        <source>triggered by</source>
        <translation>gestartet durch</translation>
    </message>
    <message>
        <source>to run</source>
        <translation>startet </translation>
    </message>
    <message>
        <source>dbus-activated</source>
        <translation>dbus-aktiviert</translation>
    </message>
    <message>
        <source>oneshot</source>
        <translation>einmalig</translation>
    </message>
    <message>
        <source>triggered</source>
        <translation>startete</translation>
    </message>
    <message>
        <source>spawned</source>
        <translation>startete</translation>
    </message>
    <message>
        <source>spawns</source>
        <translation>startet</translation>
    </message>
    <message>
        <source>Enabling</source>
        <translation type="vanished">Aktiviere</translation>
    </message>
    <message>
        <source>Disabling</source>
        <translation type="vanished">Deaktiviere</translation>
    </message>
    <message>
        <source>Starting</source>
        <translation type="vanished">Starte</translation>
    </message>
    <message>
        <source>Stopping</source>
        <translation type="vanished">Stoppe</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Starte neu</translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation>Aktiviere %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation>Deaktiviere %1</translation>
    </message>
    <message>
        <source>Starting %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation>Beende %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation>Starte %1 neu</translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message>
        <source>Failed services detected.</source>
        <translation type="vanished">Gestorbene Dienste entdeck!t</translation>
    </message>
    <message numerus="yes">
        <source>%n unit(s) ignored</source>
        <translation>
            <numerusform>eine Einheit verborgen </numerusform>
            <numerusform>%n Einheiten verborgen </numerusform>
        </translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Verbergen</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Neu Starten</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Restart Failed</source>
        <translation type="vanished">Gestorbene wiedererwecken</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Neu Laden</translation>
    </message>
    <message>
        <source>Failed units detected.</source>
        <translation>Gestorbene Einheiten entdeckt.</translation>
    </message>
    <message>
        <source>Timers</source>
        <comment>Page title unit type</comment>
        <translation>Zeitgeber</translation>
    </message>
    <message>
        <source>Paths</source>
        <comment>Page title unit type</comment>
        <translation>Pfade</translation>
    </message>
    <message>
        <source>Mounts</source>
        <comment>Page title unit type</comment>
        <translation>Einhängepunkte</translation>
    </message>
    <message>
        <source>Services</source>
        <comment>Page title unit type</comment>
        <translation>Dienste</translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation>Zu %1 wechseln</translation>
    </message>
    <message>
        <source>System Bus</source>
        <translation>Systembus</translation>
    </message>
    <message>
        <source>Session Bus</source>
        <translation>Sitzungsbus</translation>
    </message>
    <message>
        <source>Sockets</source>
        <comment>Page title unit type</comment>
        <translation>Sockets</translation>
    </message>
    <message>
        <source>Unit Name</source>
        <translation>Elementname</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation>Suche verbergen</translation>
    </message>
    <message>
        <source>Search by name</source>
        <translation>Nach Name suchen</translation>
    </message>
    <message>
        <source>Targets</source>
        <comment>Page title unit type</comment>
        <translation>Targets</translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Share…</source>
        <translation type="vanished">Teilen…</translation>
    </message>
    <message>
        <source>Copy file path to Clipboard</source>
        <translation type="vanished">Dateipfad in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>Copy contents to Clipboard</source>
        <translation type="vanished">Dateiinhalt in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>Share as Unit…</source>
        <translation type="vanished">Als Element teilen…</translation>
    </message>
    <message>
        <source>Share as Text…</source>
        <translation>Als Text teilen…</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation>Öffnen…</translation>
    </message>
    <message>
        <source>Share Unit File…</source>
        <translation type="vanished">Teile Datei…</translation>
    </message>
    <message>
        <source>Path copied to clipboard</source>
        <translation type="vanished">Pfad nun in der Zwischenablage</translation>
    </message>
    <message>
        <source>Contents copied to clipboard</source>
        <translation type="vanished">Inhalt nun in der Zwischenablage</translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation>%1 in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation>Dateipfad</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation>%1 nun in der Zwischenablage</translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation>Inhalt</translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation>%1 teilen…</translation>
    </message>
    <message>
        <source>Unit File</source>
        <comment>argument for &apos;Share&apos;</comment>
        <translation>Elementdatei</translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiviere %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Deaktiviere %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Starte %1 neu</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Beende %1</translation>
    </message>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation>%1 erfolgreich</translation>
    </message>
    <message>
        <source>Success:</source>
        <translation>Erfolgreich:</translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation>%! fehlgeschlagen</translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation>Fehler:</translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation>Preset %1</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="vanished">Aktiviere</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation>Aktiviere %1</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="vanished">Deaktiviere</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation>Deaktiviere %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Starten</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation>%1 starten</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation>%1 beenden</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Neu Starten</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation>%1 neu starten</translation>
    </message>
    <message>
        <source>%2 for unit %3</source>
        <translation type="vanished">%2 für Element %3</translation>
    </message>
    <message>
        <source>%1 %2 for unit %3</source>
        <translation type="vanished">%1 %2 für Element %3</translation>
    </message>
    <message>
        <source>%2 for unit %3</source>
        <comment>successful message, e.g. &apos;start for unit foo&apos;</comment>
        <translation>%2 für Element %3</translation>
    </message>
    <message>
        <source>%1 %2 for unit %3</source>
        <comment>failure message, e.g. &apos;starting failed for unit foo&apos;</comment>
        <translation>%1 %2 für Element %3</translation>
    </message>
    <message>
        <source>DBus service %1 ready</source>
        <translation>DBus-Dienst</translation>
    </message>
</context>
</TS>
