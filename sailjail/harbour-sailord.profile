# -*- mode: sh -*-

# Firejail profile for harbour-sailord

# x-sailjail-translation-catalog = harbour-sailord
# x-sailjail-translation-key-description = permission-la-dbus
# x-sailjail-description = Systemd D-Bus permissions
# x-sailjail-translation-key-long-description = permission-la-dbus_description
# x-sailjail-long-description = Allows access to systemd DBus interfaces


## PERMISSIONS
# x-sailjail-permission = Notifications
include /etc/sailjail/permissions/Notifications.permission

### D-Bus
dbus-user filter
dbus-user.talk org.freedesktop.DBus
dbus-user.call org.freedesktop.DBus=org.freedesktop.DBus@/*
dbus-user.broadcast org.freedesktop.DBus=org.freedesktop.DBus@/*

# BEG systemd manager and related:.system
dbus-system.talk org.freedesktop.systemd1
dbus-system.broadcast org.freedesktop.systemd1=org.freedesktop.systemd1.*@/*
dbus-system.call org.freedesktop.systemd1=org.freedesktop.systemd1@/*
dbus-system.call *=org.freedesktop.systemd1.Manager@/*
dbus-system.call *=org.freedesktop.systemd1.Unit@/*
dbus-system.call *=org.freedesktop.systemd1.Service@/*
dbus-system.call *=org.freedesktop.systemd1.Socket@/*
dbus-system.call *=org.freedesktop.systemd1.Target@/*
dbus-system.call *=org.freedesktop.systemd1.Mount@/*
dbus-system.call *=org.freedesktop.systemd1.Timer@/*
#dbus-system.call *=org.freedesktop.systemd1.Swap@/*
dbus-system.call *=org.freedesktop.systemd1.Path@/*
#dbus-system.call *=org.freedesktop.systemd1.Slice@/*
dbus-system.call *=org.freedesktop.systemd1.Job@/*



# BEG systemd manager and related
dbus-user.talk org.freedesktop.systemd1
dbus-user.broadcast org.freedesktop.systemd1=org.freedesktop.systemd1.*@/*
dbus-user.call org.freedesktop.systemd1=org.freedesktop.systemd1@/*
dbus-user.call *=org.freedesktop.systemd1.Manager@/*
dbus-user.call *=org.freedesktop.systemd1.Unit@/*
dbus-user.call *=org.freedesktop.systemd1.Service@/*
dbus-user.call *=org.freedesktop.systemd1.Socket@/*
dbus-user.call *=org.freedesktop.systemd1.Target@/*
dbus-user.call *=org.freedesktop.systemd1.Mount@/*
dbus-user.call *=org.freedesktop.systemd1.Timer@/*
#dbus-user.call *=org.freedesktop.systemd1.Swap@/*
dbus-user.call *=org.freedesktop.systemd1.Path@/*
#dbus-user.call *=org.freedesktop.systemd1.Slice@/*
dbus-user.call *=org.freedesktop.systemd1.Job@/*


# END systemd manager and related
