/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0

/*
 *  see https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
 */
DockedPanel { id: panel
    z: 10 // this fixes transparency and focus problems
    property string path: ""

    Rectangle {
        clip: true
        anchors.fill: parent
        anchors.centerIn: parent
        radius: Theme.paddingSmall
        property color backgroundColor: !app.isSessionBus ? Theme.rgba(Theme.highlightDimmerFromColor(Theme.errorColor, Theme.colorScheme), Theme.opacityOverlay) : Theme.secondaryHighlightColor
        gradient: Gradient {
            GradientStop { position: 0; color: Theme.rgba(Theme.highlightDimmerFromColor(backgroundColor, Theme.colorScheme), Theme.opacityOverlay) }
            GradientStop { position: 2; color: Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityOverlay) }
        }

    }
    Separator {
        anchors {
            verticalCenter: parent.top
            horizontalCenter: parent.horizontalCenter
            bottomMargin: Theme.paddingMedium
        }
        width: parent.width ; height: Theme.paddingSmall
        color: Theme.lightPrimaryColor;
        horizontalAlignment: Qt.AlignHCenter
    }

    Loader { id: loader
        active: parent.visible && parent.path != ""
        width: parent.width
        anchors.fill:parent
        anchors.top:parent.top
        sourceComponent:  SilicaFlickable { id: pflick
            contentHeight: content.height
            property string unitType: (!!unit.id) ? unit.id.split(".").pop() : ""
            BackgroundItem {
                anchors.fill: parent
                property bool copied: false
                onPressAndHold: {
                    if (copied) {
                        try {
                          var u = JSON.stringify(unit,null,2)
                          var d = "{}";
                          if (unitType == "service") d = JSON.stringify(svc,null,2);
                          if (unitType == "path")    d = JSON.stringify(pth,null,2);
                          if (unitType == "timer")   d = JSON.stringify(tmr,null,2);
                          if (unitType == "socket")  d = JSON.stringify(soc,null,2);
                          if (unitType == "mount")   d = JSON.stringify(mnt,null,2);
                          var t = "[\n" + u + ",\n" + d + "\n]\n";
                          Clipboard.text = t;
                          app.popup(qsTr("%1 copied to clipboard").arg(qsTr("unit details", "parameter for 'copied to clipboard' popup message")),t);
                            console.info("Unit details:\n", t)
                        } catch(error) {
                            app.failMsg("Copying",error)
                        }
                    } else {
                        Clipboard.text = unit.id
                        app.popup(qsTr("%1 copied to clipboard").arg(qsTr("unit name", "parameter for 'copied to clipboard' popup message")),qsTr("Long press again to copy detailed info"));
                    }
                    copied = !copied
                }
            }
            UnitData {      id: unit; path: panel.path ; wantSystemBus: app.isSystemBus }
            ServiceData {   id: svc;  path: panel.path ; wantSystemBus: app.isSystemBus }
            MountData {     id: mnt;  path: panel.path ; wantSystemBus: app.isSystemBus }
            PathData {      id: pth;  path: panel.path ; wantSystemBus: app.isSystemBus }
            TimerData {     id: tmr;  path: panel.path ; wantSystemBus: app.isSystemBus }
            SocketData {    id: soc;  path: panel.path ; wantSystemBus: app.isSystemBus }
            Flow { id: content
                width: parent.width
                bottomPadding: Theme.paddingLarge
                Label { id: desc
                    width: parent.width
                    anchors.topMargin: Theme.paddingMedium
                    anchors.leftMargin: Theme.horizontalPageMargin
                    anchors.rightMargin: Theme.horizontalPageMargin
                    text: unit.description
                    color: Theme.secondaryColor
                    //font.pixelSize: Theme.fontSizeLarge
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                }
                Column { id: reloadwarning
                    visible: unit.needDaemonReload
                    width: parent.width - Theme.horizontalPageMargin
                    anchors.topMargin: Theme.paddingMedium
                    Label {
                        width: parent.width
                        //anchors.leftMargin: Theme.horizontalPageMargin
                        //anchors.rightMargin: Theme.horizontalPageMargin
                        wrapMode: Text.Wrap
                        text: Theme.highlightText(
                            "⚠ The unit file, source configuration file or drop-ins changed on disk. Information may be inaccurate until you reload the daemon.",
                            "⚠",
                            Theme.highlightFromColor("yellow", Theme.colorScheme)
                        )
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: Theme.fontSizeSmall
                    }
                    Button {
                        text: "Daemon reload"
                        icon.source: "image://theme/icon-m-refresh"
                        anchors.horizontalCenter: parent.horizontalCenter
                        onClicked:  { dbus.daemon_reload(); panel.hide() }
                    }
                }
                // Generic unit information
                Column { id: unitcol
                    width: isPortrait ? parent.width : parent.width/2
                    spacing: 0

                    SectionHeader { text: qsTr("Unit Properties") }
                    // most of these should be untranslated, techical terms:
                    UnitDetail { label: "ID";             value: unit.id }
                    UnitDetail { label: qsTr("Other Names"); visible: value.length > 0;   value: (unit.names.length > 1) ? unit.names.splice(0,1) : "" }
                    UnitDetail { label: "Unit Path";      value: (unit.sourcePath.length > 0) ? unit.sourcePath : ( (unit.fragmentPath.length > 0) ? unit.fragmentPath : "" )}
                    //UnitDetail { label: "User";     value: unit.user }
                    //UnitDetail { label: "Group";    value: unit.group }
                    UnitDetail { label: "Unit Status";    value: unit.unitFileState}
                    UnitDetail { label: "Preset";         value: unit.unitFilePreset}
                    UnitDetail { label: "State";          value: unit.activeState}
                    UnitDetail { label: "SubState";       value: unit.subState}
                    /*
                     * Systemd times are in microseconds (us), javascript in milliseconds
                     */
                    UnitDetail { label: "Changed";  value: (unit.stateChangeTimestamp != 0) ? new Date(Math.floor(unit.stateChangeTimestamp/1000)) : "never"}
                    Separator {color: Theme.secondaryHighlightColor; width: parent.width; horizontalAlignment: Qt.AlignRight }
                }
                Loader { id: svcLoader
                    active: unitType == "service"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column { id: servicecol
                        SectionHeader { text: qsTr("Service Properties") }
                        UnitDetail { label: "Type";           value: svc.type }
                        UnitDetail { label: "Exec Start";
                            value: {
                                const s=[];
                                for (var i=0; i<svc.execStart.length; i++) {
                                    s.push(svc.execStart[i][1].join(" "));
                                }
                                return s.join('\n');
                            }
                        }
                        UnitDetail { label: "Exec Stop";
                            value: {
                                const s=[];
                                for (var i=0; i<svc.execStop.length; i++) {
                                    s.push(svc.execStop[i][1].join(" "));
                                }
                                return s.join('\n');
                            }
                        }
                        UnitDetail { label: "Exit Type";      value: svc.exitType }
                        UnitDetail { label: "StatusText";     value: svc.statusText}
                        UnitDetail { label: "Exec Code";      value: svc.execMainCode}
                        UnitDetail { label: "Exec Status";    value: svc.execMainStatus + "/" + svc.result.toUpperCase(); }
                        //UnitDetail { label: "Result";         value: svc.result}
                    }
                }
                Loader { id: timerLoader
                    active: pflick.unitType == "timer"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Timer Properties") }
                        UnitDetail { label: "Persistent";                 value: tmr.persistent}
                        UnitDetail { label: "Wake System";                value: tmr.wakeSystem}
                        UnitDetail { label: "Remain After Elapse";        value: tmr.remainAfterElapse}
                        // this is always "OnCalendar" - "for now"
                        UnitDetail { label: tmr.timersCalendar[0][0];
                            value: {
                                const s = [];
                                for (var i=0; i<tmr.timersCalendar.length;i++){
                                    s.push(tmr.timersCalendar[i][1])
                                }
                                return s.join('\n');
                            }
                        }
                        UnitDetail { label: "Next Elapse";          value: (tmr.nextElapseUSecRealtime != 0) ? new Date(Math.floor(tmr.nextElapseUSecRealtime/1000)) : "never" }
                        UnitDetail { label: "Last Trigger";         value: (tmr.lastTriggerUSec != 0) ? new Date(Math.floor(tmr.lastTriggerUSec/1000)) : "never" }
                        //UnitDetail { label: "Accuracy";             value: (tmr.accuracyUSec/1000000).toFixed(2)}
                        //UnitDetail { label: "Randomized Delay";     value: (tmr.randomizedDelayUSec/1000000).toFixed(2)}
                        UnitDetail { label: "Accuracy";             value: Math.round(tmr.accuracyUSec/1000000)+"s"}
                        UnitDetail { label: "Randomized Delay";     value: Math.round(tmr.randomizedDelayUSec/1000000)+"s"}
                        UnitDetail { label: "Fixed Random Delay";   value: tmr.fixedRandomDelay}
                        UnitDetail { label: "Result";               value: tmr.result.toUpperCase()}
                    }
                }
                Loader { id: socketLoader
                    active: pflick.unitType == "socket"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Socket Properties") }
                        //UnitDetail { label: "Type";             value: soc.type }
                        UnitDetail { label: "IPv6 only";        value: soc.bindIPv6Only }
                        UnitDetail { label: "Device";           value: soc.bindToDevice}
                        UnitDetail { label: "Listen";           value: soc.listen.map(function(e) { return e[0] + " " + e[1] }).join("\n") }
                        UnitDetail { label: "Service";          value: soc.service}
                        //UnitDetail { label: "Accept";           value: soc.accept}
                        UnitDetail { label: "Connections";      value: soc.nConnections}
                        UnitDetail { label: "Accepted";         value: soc.nAcceped}
                        UnitDetail { label: "Refused";          value: soc.nRefused}
                        UnitDetail { label: "Result";           value: soc.result.toUpperCase()}
                    }
                }
                Loader { id: mountLoader
                    active: pflick.unitType == "mount"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Mount Properties") }
                        UnitDetail { label: "Type";           value: mnt.type }
                        UnitDetail { label: "Where";          value: mnt.where }
                        UnitDetail { label: "What";           value: mnt.what}
                        UnitDetail { label: "Options";        value: mnt.options}
                        UnitDetail { label: "Result";         value: mnt.result.toUpperCase()}
                    }
                }
                Loader { id: pathLoader
                    active: pflick.unitType == "path"
                    width: isPortrait ? parent.width : parent.width/2
                    sourceComponent: Column {
                        SectionHeader { text: qsTr("Path Properties") }
                        //UnitDetail { label: "Paths";          value: JSON.stringify(pth.paths) }
                        UnitDetail { label: "Paths";
                            value: {
                                const s = [];
                                for (var i=0; i<pth.paths.length;i++){
                                    s.push(pth.paths[i][0] + ': ' + pth.paths[i][1])
                                }
                                return s.join('\n');
                            }
                        }
                        UnitDetail { label: "Make Directory"; value: pth.makeDirectory}
                        //UnitDetail { label: "Mode";           value: pth.directoryMode}
                        UnitDetail { label: "Result";         value: pth.result.toUpperCase()}
                    }
                }
                Column { id: depcol
                    width: isPortrait ? parent.width : parent.width/2
                    spacing: 0

                    Separator {color: Theme.secondaryHighlightColor; width: parent.width; horizontalAlignment: Qt.AlignRight }
                    SectionHeader { text: qsTr("Dependencies")}
                    UnitDetail { label: "Before";         visible: value.length > 0; value: unit.before.join('\n')}
                    UnitDetail { label: "After";          visible: value.length > 0; value: unit.after.join('\n')}
                    UnitDetail { label: "Requires";       visible: value.length > 0; value: unit.requires.join('\n')}
                    UnitDetail { label: "RequiredBy";     visible: value.length > 0; value: unit.requiredBy.join('\n')}
                    UnitDetail { label: "Wants";          visible: value.length > 0; value: unit.wants.join('\n')}
                    UnitDetail { label: "WantedBy";       visible: value.length > 0; value: unit.wantedBy.join('\n')}
                    UnitDetail { label: "Triggers";       visible: value.length > 0; value: unit.triggers.join('\n')}
                    UnitDetail { label: "TriggeredBy";    visible: value.length > 0; value: unit.triggeredBy.join('\n')}
                }
                Column { id: buttcol // teehehehehee!
                    width: isPortrait ? parent.width : parent.width/2
                    spacing: 0
                    SectionHeader { text: qsTr("Unit File") }
                    ButtonLayout { id: buttons
                        width: parent.width
                        property string unitPath:   (unit.sourcePath.length > 0) ? unit.sourcePath : ( (unit.fragmentPath.length > 0) ? unit.fragmentPath : "" )
                        SecondaryButton {
                            text: qsTr("Copy Path")
                            icon.source: "image://theme/icon-m-clipboard"
                            enabled: parent.unitPath.length > 0
                            onClicked: {
                                Clipboard.text = parent.unitPath;
                                app.popup(qsTr("%1 copied to clipboard").arg(qsTr("unit file path", "parameter for 'copied to clipboard' popup message")),parent.unitPath);
                            }
                        }
                        SecondaryButton {
                            text: qsTr("Show Content")
                            icon.source: (Theme.colorScheme == Theme.LightOnDark) ? "image://theme/icon-m-file-document-light" : "image://theme/icon-m-file-document-dark"
                            enabled: parent.unitPath.length > 0
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl("../pages/ViewUnitFile.qml"), { "unitName": unit.id , "fileName": parent.unitPath } )
                            }
                        }
                    }
                }
            }
        }
    }
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
