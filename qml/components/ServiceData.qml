/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import Nemo.DBus 2.0

/*
 *  see https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
 */
UnitDataBase  {
    id: svc
    //bus: DBus.SessionBus
    //service: "org.freedesktop.systemd1"
    iface: "org.freedesktop.systemd1.Service"
    //path: panel.path
    //propertiesEnabled: true
    property string type
    property string exitType
    property string statusText
    property string result
    property string execMainStatus
    property string execMainCode
    property var execStart
    property var execStop
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
