/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0

DetailItem {
    alignment: Qt.AlignLeft
    readonly property string breaker: "NNNN-NN-NNTTT:TT:TT" // a date string
    forceValueBelow: value.length > breaker.length 
    leftMargin: Theme.paddingLarge
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
