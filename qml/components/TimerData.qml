/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Nemo.DBus 2.0

/*
 *  see https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
 */
UnitDataBase  {
    //bus: DBus.SessionBus
    //service: "org.freedesktop.systemd1"
    iface: "org.freedesktop.systemd1.Timer"
    //path: panel.path
    //propertiesEnabled: true
    /*
     * array of struct
     */
      property var timersMonotonic
      property var timersCalendar

      // NOTE: use strings for dbus 't' values, ints don't work.
      property string result
      property string nextElapseUSecRealtime
      //property string nextElapseUSecMonotonic
      property string lastTriggerUSec
      //property string lastTriggerUSecMonotonic
      property int accuracyUSec
      property int randomizedDelayUSec
      property bool fixedRandomDelay
      property bool persistent
      property bool wakeSystem
      property bool remainAfterElapse

}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
