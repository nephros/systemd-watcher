/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Nemo.DBus 2.0

/*
 *  see https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
 */
UnitDataBase  {
    //bus: DBus.SessionBus
    //service: "org.freedesktop.systemd1"
    iface: "org.freedesktop.systemd1.Mount"
    //path: panel.path
    //propertiesEnabled: true
    property string type
    property string where
    property string what
    property string options
    property string result
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
