/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Nemo.DBus 2.0

/*
 *  see https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
 */
UnitDataBase  {
    id: unit
    //bus: DBus.SessionBus
    //service: "org.freedesktop.systemd1"
    iface: "org.freedesktop.systemd1.Unit"
    //path: panel.path
    //propertiesEnabled: true
    property string id
    property var names: []
    property string description
    //property bool transient
    property string activeState
    property string subState
    property string stateChangeTimestamp
    //property string stateChangeTimestampMonotonic
    property string unitFileState
    property string unitFilePreset
    //onUnitFileStateChanged: console.debug("unitstate: ", unitFileState );
    //onUnitFilePresetChanged: console.debug("unitpreset: ", unitFilePreset );
    property string user
    property string group

    property string sourcePath
    property string fragmentPath
    //property var dropInPaths: []

    property bool canStart
    property bool canStop
    property bool canReload

    property bool needDaemonReload

    property var before: []
    property var after: []
    property var requires: []
    property var requiredBy: []
    property var wants: []
    property var wantedBy: []
    property var triggers: []
    property var triggeredBy: []
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
