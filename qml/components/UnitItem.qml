/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0

ListItem { id: item

    /*
     * while we can get the unitfilestate from systemd, that involves a callback.
     * so set up a callback and property here:
     *
     */
    property string unitFileState: "unset"
    function setUnitFileState(s) {
        unitFileState = s;
    }
    onVisibleChanged: visible
                        && (unitFileState === "unset")
                        && (state != "mount")
                        && (model.unit)
            ? dbus.getUnitState(model.unit, setUnitFileState) : 0

    // cf. app.supportedUnits and page.states
    state: (!!custom.unitType) ? custom.unitType : "service"
    states: [
        // default: "" == "service"
        State { name: "service"
            PropertyChanges { target: extraData; active: true ; sourceComponent: serviceDataComp }
        },
        State { name: "timer"
            PropertyChanges { target: extraData; active: true ; sourceComponent: timerDataComp }
        },
        State { name: "path"
            PropertyChanges { target: extraData; active: true ; sourceComponent: pathDataComp }
        },
        State { name: "socket"
            PropertyChanges { target: extraData; active: true ; sourceComponent: socketDataComp }
        },
        State { name: "target"
            PropertyChanges { target: extraData; active: true ; sourceComponent: targetDataComp }
        },
        State { name: "mount"
        }
    ]

    contentHeight: content.height
    hidden: false

    // open DetailsPanel
    onClicked: { if (service.length > 0) {details.path = service; details.show()} else { return } }


    Component {  id: emptyMenu
        ContextMenu {
            MenuLabel { text: qsTr("No actions available."); }
        }
    }
    Component {  id: defaultMenu
        ContextMenu {
            MenuItem { text: qsTr("Ignore this session"); onClicked: { hidden=true ; app.addIgnore({ "unit": model.unit }) } }
            MenuItem { text: qsTr("Ignore permanently");  onClicked: { hidden=true ; app.addIgnorePerm({ "unit": model.unit }); app.saveIgnore() } }
        }
    }
    Component { id: advancedMenu
        ContextMenu {
            MenuItem { text: qsTr("Ignore this session"); onClicked: { hidden=true ; app.addIgnore({ "unit": model.unit }) } }
            MenuItem { text: qsTr("Ignore permanently");  onClicked: { hidden=true ; app.addIgnorePerm({ "unit": model.unit }); app.saveIgnore() } }
            MenuItem { text: qsTr("Enable");                                           enabled: (unitFileState == "disabled");                              onDelayedClick: { enableUnit()  } }
            MenuItem { text: qsTr("Disable");                                          enabled: (unitFileState == "enabled");                               onDelayedClick: { disableUnit() } }
            MenuItem { text: item.state == "mount" ? qsTr("Mount")   : qsTr("Start");  enabled: (model.status == "inactive" || model.status == "failed");   onDelayedClick: { startUnit()   } }
            MenuItem { text: item.state == "mount" ? qsTr("Unmount") : qsTr("Stop");   enabled: (model.status == "active");                                 onDelayedClick: { stopUnit()    } }
            MenuItem { text: item.state == "mount" ? qsTr("Remount") : qsTr("Restart");                                                                     onDelayedClick: { restartUnit() } }
        }
    }

    // define the functions here so they are not in ContextMenu scope
    //function enableUnit()   { var u = unit; remorseAction(qsTr("Enabling %1"  ).arg(u), function() { dbus.enable(u)  }) }
    //function disableUnit()  { var u = unit; remorseAction(qsTr("Disabling %1" ).arg(u), function() { dbus.disable(u) }) }
    //function startUnit()    { var u = unit; remorseAction(qsTr("Starting %1"  ).arg(u), function() { dbus.start(u)   }) }
    //function stopUnit()     { var u = unit; remorseAction(qsTr("Stopping %1"  ).arg(u), function() { dbus.stop(u)    }) }
    //function restartUnit()  { var u = unit; remorseAction(qsTr("Restarting %1").arg(u), function() { dbus.restart(u) }) }
    // use Remorse instead of remorseAction, so we can have multiple ones simultanuously:
    function enableUnit()   { var u = unit; Remorse.itemAction(item, qsTr("Enabling %1"  ).arg(u), function() { dbus.enable(u)  }) }
    function disableUnit()  { var u = unit; Remorse.itemAction(item, qsTr("Disabling %1" ).arg(u), function() { dbus.disable(u) }) }
    function startUnit()    { var u = unit; Remorse.itemAction(item, qsTr("Starting %1"  ).arg(u), function() { dbus.start(u)   }) }
    function stopUnit()     { var u = unit; Remorse.itemAction(item, qsTr("Stopping %1"  ).arg(u), function() { dbus.stop(u)    }) }
    function restartUnit()  { var u = unit; Remorse.itemAction(item, qsTr("Restarting %1").arg(u), function() { dbus.restart(u) }) }

    // Decide which menu to show depending on user settings
    // WARNING! Logic ahead!
    menu: (app.systemctl)
        ? advancedMenu                  // systemctl on always gets advanced
        : (                             // otherwise
            (app.ignoreAll)             // ignoreall always gets default
                ? defaultMenu           // otherwise ony show default if we are a failed unit
                : ( (model.status == "failed") ? defaultMenu : emptyMenu )
        )

    // define detail data components to shove into the loader through state changes:
    UnitData{ id: unitData; path: model.service; wantSystemBus: app.isSystemBus }
    Component{ id: timerDataComp;    TimerData{path: model.service; wantSystemBus: app.isSystemBus }}
    Component{ id: pathDataComp;     PathData{path: model.service; wantSystemBus: app.isSystemBus } }
    Component{ id: serviceDataComp;  ServiceData{path: model.service; wantSystemBus: app.isSystemBus } }
    Component{ id: socketDataComp;   SocketData{path: model.service; wantSystemBus: app.isSystemBus } }
    Component{ id: targetDataComp;   TargetData{path: model.service; wantSystemBus: app.isSystemBus } }
    Loader { id: extraData; active: false }

    Column { id: content
        width: parent.width - Theme.paddingSmall * 4 // see Rectangle...
        anchors.centerIn: parent
        //anchors.horizontalCenter: parent.horizontalCenter
        //Label { text: desc; truncationMode: TruncationMode.Fade; width: parent.width}
        Label { text: desc; truncationMode: TruncationMode.Fade; width: parent.width; font.pixelSize: Theme.fontSizeSmall}
        Label { text: unit; truncationMode: TruncationMode.Fade; color: Theme.secondaryColor; font.pixelSize: Theme.fontSizeExtraSmall}
        Row{
            width: parent.width
            spacing: Theme.paddingLarge
            Label { id: statuslabel;
                text: status
                    + "("+sub+")"
                    + ( ( (sub == "failed") || (sub == "exited") ) ? "[" + extraData.item.execMainCode + "]" :  "" )
                    + ((unitData.needDaemonReload === true) ? Theme.highlightText(" ⚠", "⚠",  Theme.highlightFromColor("yellow", Theme.colorScheme)) : "")
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }
            // show some additional information depending on unit type and state
            Label { id: extralabel
                visible: text != "" && !nextlabel.visible
                width: content.width - statuslabel.width - Theme.horizontalPageMargin
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade;
                text: (extraData.status == Loader.Ready) ? lblcontent : ""
                property string lblcontent: {
                    switch (item.state) {
                        case "service":
                            // dbus or oneshot
                            if (extraData.item.type == "oneshot")   return qsTr("oneshot");
                            if (extraData.item.type == "dbus" )     return qsTr("dbus-activated");
                            // triggered by
                            if (unitData.triggeredBy.length > 0) {
                                const v = unitData.triggeredBy.map( function(e){
                                    return e.split(".").pop();
                                }
                                ).sort().join(", ");
                                return qsTr("triggered by") + " " + v;
                            }
                            break;
                        case "path":
                            // triggers
                            if ( unitData.triggers.length > 0 )
                                return ( (sub == "waiting") ? qsTr("to run") : qsTr("triggered") ) + " " + unitData.triggers.join();
                            break;
                        case "socket":
                            // triggers
                            if ( unitData.triggers.length > 0 )
                                return ( (sub == "running") ? qsTr("spawned") : qsTr("spawns") ) + " " + unitData.triggers.join();
                            break;
                        default:
                            return "";
                    }
                    return ""; // don't be undefined
                }
            }
            // label for next timer trigger
            Label { id: nextlabel
                visible: item.state == "timer" && sub == "waiting" && extraData.status == Loader.Ready
                property date t: visible ? new Date(Math.floor(extraData.item.nextElapseUSecRealtime/1000)) : new Date()
                text: qsTr("next") + ": " + (visible ? ( (extraData.item.nextElapseUSecRealtime!=0) ? Format.formatDate(t, Formatter.TimepointRelative) : "never") : "")
                width: isPortrait ? content.width - statuslabel.width - Theme.horizontalPageMargin : content.width/2
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }
            /*
            Label { id: lastlabel
                visible: item.state == "timer" && sub === "waiting"; 
                property date t: new Date(Math.floor(extraData.item.lastTriggerUSec/1000)) 
                text: "last: " + Format.formatDate(t, Formatter.TimepointRelativeCurrentDay)
                horizontalAlignment: Text.alignRight
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }
            */
        }
    }
    // status indicator
    Rectangle {
        color: Theme.highlightFromColor(baseColor, Theme.colorScheme)
        // <blink>
        ColorAnimation on color {
            running: ( ( status === "activating" ) || (status === "deactivating" ) )
            from: "transparent"
            to: Theme.highlightFromColor("yellow", Theme.colorScheme)
        }
        property color baseColor: switch(status) {
            case "active":    return "green"; break;
            case "inactive":  return "grey";  break;
            case "failed":    return "red";   break;
            default:          return "transparent";
        }
        height: parent.height/2
        width: Theme.paddingSmall
        anchors.horizontalCenter: parent.left
        anchors.top: content.top
    }
    Rectangle {
        color: Theme.highlightFromColor(baseColor, Theme.colorScheme)
        // <blink>
        ColorAnimation on color {
            running: ( status === "auto-restart" )
            from: "transparent"
            to: Theme.highlightFromColor("yellow", Theme.colorScheme)
        }
        property color baseColor: switch (sub) {
            case "failed":      return "darkred"; break;
            case "dead":
            case "elapsed":
            case "exited":      return "darkgrey"; break;
            case "mounted":     // mounts ;)
            case "active":      // targets are active(active)
            case "running":     return "darkgreen"; break;
            case "listening":
            case "waiting":     return "darkorange"; break
            default:            return "transparent";
        }
        height: parent.height/2
        width: Theme.paddingSmall
        anchors.horizontalCenter: parent.left
        anchors.bottom: content.bottom
    }
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
