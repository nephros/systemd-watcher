/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import "../components"

Page {
    id: page

    backgroundColor: !app.isSessionBus ? Theme.rgba(Theme.highlightDimmerFromColor(Theme.errorColor, Theme.colorScheme), Theme.opacityOverlay) : "transparent"

    /* control search field visible */
    property bool searchActive: false

    // cf. app.supportedUnits
    // default: "" == "service"
    states: [
        State { name: "timer"
            PropertyChanges {  target: view; title: qsTr("Timers","Page title unit type") }
            PropertyChanges {  target: page; objectName: "TimerPage"; nextState: app.supportedUnits[2] }
            PropertyChanges {  target: page; unitModel: timerModel }
            PropertyChanges {  target: checkTimer; running: false }
        },
        State { name: "path"
            PropertyChanges {  target: view; title: qsTr("Paths","Page title unit type");  }
            PropertyChanges {  target: page; objectName: "PathPage"; nextState: app.supportedUnits[3] }
            PropertyChanges {  target: page; unitModel: pathModel }
            PropertyChanges {  target: checkTimer; running: false }
        },
        State { name: "socket"
            PropertyChanges {  target: view; title: qsTr("Sockets","Page title unit type");  }
            PropertyChanges {  target: page; objectName: "SocketPage"; nextState: app.supportedUnits[4]; }
            PropertyChanges {  target: page; unitModel: socketModel }
            PropertyChanges {  target: checkTimer; running: false }
        },
        State { name: "target"
            PropertyChanges {  target: view; title: qsTr("Targets","Page title unit type");  }
            PropertyChanges {  target: page; objectName: "TargetPage"; nextState: app.supportedUnits[5]; }
            PropertyChanges {  target: page; unitModel: targetModel }
            PropertyChanges {  target: checkTimer; running: false }
        },
        State { name: "mount"
            PropertyChanges {  target: view; title: qsTr("Mounts","Page title unit type"); }
            PropertyChanges {  target: page; objectName: "MountPage"; nextState: ""; forwardNavigation : false }
            PropertyChanges {  target: page; unitModel: mountModel }
            PropertyChanges {  target: checkTimer; running: false }
        }
    ]

    property string nextState: app.supportedUnits[1]
    /*
     * push another page if applicable. careful, this may cause lipstick crashes!
     */

    Connections {
        onStatusChanged: {
            if (pageStack.busy) {
                pageStack.busyChanged.connect(pushNextPage)
            } else {
                    pushNextPage()
            }
        }
    }
    function pushNextPage() {
        if (
            status === PageStatus.Active
            && pageStack.nextPage() === null
            && !pageStack.busy
            && app.supportedUnits.indexOf(nextState) >=0
        ) {
            pageStack.pushAttached("UnitPage.qml", { "state": nextState } );
            pageStack.busyChanged.disconnect(pushNextPage);
        }
    }

    onStatusChanged: {
       if ( status != PageStatus.Active ) {
           if ( details.open ) details.hide();
           view.currentIndex = -1;
       }
    }
    property ListModel unitModel: serviceModel
    property var failed: []

    Timer { id: checkTimer
        interval: (app.checkInterval ? Number(app.checkInterval) : 120) * 1000
        repeat: true
        running: true
        triggeredOnStart: true
        onTriggered: {
            console.debug("check triggered (" + interval +")");
            if (unitModel.count <= 0) return;
            failed = [];
            const o = {};
            for (var i=0; i< unitModel.count; i++) {
                o = unitModel.get(i);
                if (app.contains(ignore,"unit",o.unit)) continue;
                if ( (o.sub === "failed") ) failed.push(o.unit);
            }
            if (failed.length > 0)
                sysAlert();
        }
    }
    function sysAlert() {
        if (!app.notify) return;
        console.debug("alert!");
        notification.previewSummary = qsTr("Failed units detected.");
        notification.previewBody = "\n" + failed.join("\n");
        notification.summary = qsTr("Failed units detected.");
        notification.body = failed.join("\n") //+ "<br />" + "(" + qsTr("%n service(s) ignored","",app.ignore.count) +")";
        notification.subText = qsTr("%n unit(s) ignored","", app.ignore.count);
        notification.icon = "icon-lock-warning";
        notification.itemCount = failed.length;
        if (app.notifySticky) notification.replacesId = (notification.replacesId) ? notification.replacesId : 0
        notification.publish();
    }
    Notification { id: notification; isTransient: false; appName: Qt.application.name; appIcon: "harbour-sailord"; category: "device.removed" }

    DetailsPanel { id: details
        dock: Dock.Bottom
        modal: true
        animationDuration : 250
        //height: content.height
        height:  page.height * 2/3
        width: parent.width
    }

    /* draw a red border and background if we are in System Bus mode */
    Rectangle {
        anchors.fill: parent
        z: -1
        visible: !app.isSessionBus
        color: "transparent"
        border.color: !app.isSessionBus ? Theme.secondaryHighlightFromColor(Theme.errorColor, Theme.colorScheme) : "transparent"
        border.width: Theme.paddingLarge / 5
        opacity: visible? 1.0 : 0.0 // need this because we can't animate visible, and can't put a behavior on border
        Behavior on opacity { PropertyAnimation {duration: 2000; target: border; property: color; easing.type: Easing.OutBounce } }
    }

    SilicaFlickable { id: flick
        anchors.fill: parent
        PageHeader { id: head; title: view.title; description:  app.ignore.count > 0 ? qsTr("%n unit(s) ignored","", app.ignore.count) : "" }
        SearchField { id: nameSearch
            anchors.top: head.bottom
            active: page.searchActive
            placeholderText: qsTr("Unit Name")
            inputMethodHints: Qt.ImhNoAutoUppercase
            acceptableInput: text.length >= 3
            EnterKey.enabled: acceptableInput
            EnterKey.iconSource: "image://theme/icon-m-search"
            EnterKey.onClicked: view.findNames(text)
            canHide: true
            onHideClicked: page.searchActive = !page.searchActive
            Separator { anchors.verticalCenter: parent.bottom; width: parent.width; color: Theme.primaryColor;}
        }

        SilicaListView { id: view
            width: parent.width - Theme.horizontalPageMargin
            height: parent.height - head.height - nameSearch.height
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: nameSearch.visible ? nameSearch.bottom : head.bottom

            property string title: qsTr("Services","Page title unit type")
            spacing: Theme.paddingLarge
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            /*
             * workaround for the ContextMenu resizing the highlight item
             * this looks especially ugly when the item is closed
             */
            property ContextMenu dummyMenu: ContextMenu{}
            highlightResizeDuration: dummyMenu._openAnimationDuration
            highlightResizeVelocity: 0
            highlight: Component {
                Rectangle {
                    color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint);
                    border.color: Theme.highlightBackgroundColor
                    radius: Theme.paddingSmall
                    // unbind/clamp height so it does not change with contextmenu
                    // this works but causes a binding loop:
                    //height: Math.min( ListView.highlightItem.height, ListView.view.currentItem.contentHeight )
                }
            }
            clip: true
            highlightFollowsCurrentItem: true
            footer: Item{ height: Theme.paddingLarge }
            section {
                //property: count > 10 ? "unit" : ""
                // this is nice but confuses the positioner
                //property: view.contentHeight > page.height*2 ? "unit" : ""
                property: count > 15 ? "unit" : ""
                criteria: ViewSection.FirstCharacter
                delegate: SectionHeader {text: section.toUpperCase()}
            }
            model: unitModel
            delegate: UnitItem{
                onMenuOpenChanged: {
                    // handle search field focus so the VKB doesn't interfere, see issue #25
                    if (menuOpen && nameSearch.focus) nameSearch.focus = false
                    if (!menuOpen && nameSearch.visible) nameSearch.focus = true
                }
            }

            /* search helpers */
            function findNames(name) {
                //var pos = findIndex(unitModel, function(item) { return  (item.desc.match(name) || item.unit.match(name)) });
                var pos = findIndex(unitModel, function(item) { var re = new RegExp(name, "gi" ); return  (re.test(item.desc) || re.test(item.unit)) });
                positionViewAtIndex(pos, ListView.Center);
                currentIndex = pos;
            }
            /* find something, return index */
            function findIndex(model, criteria) {
                for(var i = ((currentIndex > 0) ? currentIndex+1 : 0); i < model.count; ++i) if (criteria(model.get(i))) return i
                return -1
            }
        }

        PullDownMenu { id: pdp
            MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
            MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
            //MenuItem { text: qsTr("Restart Failed"); onClicked: { Remorse.popupAction(page, qsTr("Try to restart failed"), function() { dbus.restartFailed() }) } }
            MenuItem {
                visible: app.systembus
                text: (!!app.isSessionBus && app.systembus) ? qsTr("Switch to %1").arg(qsTr("System Bus")) : qsTr("Switch to %1").arg(qsTr("Session Bus"))
                onDelayedClick: { dbus.switchBus() }
            }
            MenuItem { text: qsTr("Refresh"); onDelayedClick: { app.refresh() } }
            MenuItem { text: searchActive ? qsTr("Hide search") : qsTr("Search by name") ; onClicked: searchActive = !searchActive }
        }
        VerticalScrollDecorator {}
    }

}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
