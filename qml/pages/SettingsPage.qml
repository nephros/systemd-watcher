/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    id: settingsPage

    onStatusChanged: (status === PageStatus.Deactivating) ? app.refresh() : 0

    SilicaFlickable{
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            //PageHeader{ title:  Qt.application.name + " " + qsTr("Settings", "page title")}
            PageHeader{ title: qsTr("Settings", "page title")}
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Application")
            }
            TextSwitch{ id: notifysw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.notify
                automaticCheck: true
                text: qsTr("Show Notifications")
                description: qsTr("If enabled, the app will send notifications about failed units.<br /> Use the slider below to specify how often to check.")
                onClicked: app.notify = checked
            }
            Slider {
                id: intervalSlider
                enabled: notifysw.checked
                handleVisible: enabled
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                //label: qsTr("Interval in %1","parameter is minutes or seconds").arg(qsTr("minutes"));
                label: qsTr("Check every");
                minimumValue: 1
                maximumValue: 120
                stepSize: 1
                value: app.checkInterval / 5
                //valueText: value * 5
                valueText: Format.formatDuration(value*5,Formatter.DurationShort) + " " + qsTr("min");
                onReleased: app.checkInterval = sliderValue * 5
            }
           TextSwitch{
                enabled: notifysw.checked
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.notifySticky
                automaticCheck: true
                text: qsTr("Sticky Notifications")
                description: qsTr("If enabled, the app will update a single notification (as opposed to sending a new one each time).")
                onClicked: app.notifySticky = checked
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.ignoreAll
                automaticCheck: true
                text: qsTr("Offer Ignore for all units")
                description: qsTr("If enabled, the app will offer the ignore opton for any unit (not just failed ones).")
                onClicked: app.ignoreAll = checked
            }
            SectionHeader { text: qsTr("Ignored Units:"); }
            Label {
                visible: (!pList.visible && !iList.visible)
                width: parent.width // - Theme.paddingLarge
                anchors {
                    // align to slider left
                    left: intervalSlider.left
                    leftMargin: intervalSlider.leftMargin //+ Theme.paddingLarge
                    right: intervalSlider.right
                    rightMargin: intervalSlider.rightMargin //+ Theme.paddingLarge
                    //topMargin: Theme.paddingMedium
                }
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.Wrap
                text: qsTr("No ignored units")
                horizontalAlignment: Text.alignRight
            }
            /* permanent ignore list */
            ListItem { id: pList
                visible: (app.pignore.count > 0)
                contentHeight: pCol.height
                anchors {
                    // align to slider left
                    left: intervalSlider.left
                    leftMargin: intervalSlider.leftMargin //+ Theme.paddingLarge
                    right: intervalSlider.right
                    rightMargin: intervalSlider.rightMargin //+ Theme.paddingLarge
                    //topMargin: Theme.paddingMedium
                }
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Clear permanent ignore list")
                        onClicked: { pList.remorseAction(qsTr("List Cleared."), function() { app.resetIgnorePerm() } )}
                    }
                }
                Column { id: pCol
                    width: parent.width
                    Label {
                        width: parent.width // - Theme.paddingLarge
                        font.pixelSize: Theme.fontSizeExtraSmall
                        wrapMode: Text.Wrap
                        text: qsTr("These units are permanently ignored:")
                    }
                    Repeater { id: pRep
                        model: app.pignore
                        delegate:  Label {
                            text: unit
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                        }
                    }
                }
            }
            /* session ignore list */
            ListItem { id: iList
                visible: (app.ignore.count > 0)
                contentHeight: iCol.height
                anchors {
                    // align to slider left
                    left: intervalSlider.left
                    leftMargin: intervalSlider.leftMargin //+ Theme.paddingLarge
                    right: intervalSlider.right
                    rightMargin: intervalSlider.rightMargin //+ Theme.paddingLarge
                    //topMargin: Theme.paddingMedium
                }
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Clear session ignore list")
                        onClicked: { iList.remorseAction(qsTr("List Cleared."), function() { app.ignore.clear() } )}
                    }
                }
                Column { id: iCol
                    width: parent.width
                    Label {
                        width: parent.width // - Theme.paddingLarge
                        font.pixelSize: Theme.fontSizeExtraSmall
                        wrapMode: Text.Wrap
                        text: qsTr("These units are ignored in this session:")
                    }
                    Repeater { id: iRep
                        model: app.ignore
                        delegate:  Label {
                            text: unit
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                        }
                    }
                }
            }
            SectionHeader { text: qsTr("Advanced:")}
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: !app.hybris
                automaticCheck: true
                text: qsTr("Hide Firmware mounts")
                description: qsTr("If enabled, the app will not list firmware (Android Base, Hybris) mounts.")
                onClicked: app.hybris = !checked
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: !app.boring
                automaticCheck: true
                text: qsTr("Hide uninteresting units")
                description: qsTr("If enabled, the app will not list units that are plugged, tentative, or similar.")
                onClicked: app.boring = !checked
            }
            SectionHeader { id: dangerHeader
                text: qsTr("⚠ Danger Zone 🛦🛦🛦");
                font.pixelSize: Theme.fontSizeMedium
                }
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.alignHCenter
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.Wrap
                text: qsTr( "💥 ☠ WARNING! DANGER! DRAGONS! ☠ 💥")
            }
            Label {
                width: parent.width - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignJustify
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap
                text: qsTr("This app does little to no checking whether your actions actually make sense.<br />Make sure you have a thorough understanding of how systemd units and their status work before enabling the options below.<br />If you break something, you get to keep ALL THE PIECES.")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.systembus
                automaticCheck: true
                onClicked: app.systembus = checked
                text: qsTr("Bus Switching")
                description: qsTr("If enabled, you will be able to toggle access to the System Bus.")
            }
            TextSwitch{ id: controlsw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.systemctl
                automaticCheck: true
                onClicked: app.systemctl = checked
                text: qsTr("Control Units")
                description: qsTr(
"If enabled, you will be able to start, stop, enable, disable units.<br />
                    Even if enabled, standard systemd/dbus/polkit permissions apply. This means some of the operations may not succeed, or ask for authorization.<br />
")
                }
            TextSwitch{ id: succcsw
                enabled: controlsw.checked
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.showSuccess
                automaticCheck: true
                text: qsTr("Show Success Popups")
                description: qsTr("If disabled, the app will only show popups about operation failures. Otherwise, succeess is reported as popup message.")
                onClicked: app.showSuccess = checked
            }
        }
        /*
        PullDownMenu {
            //MenuLabel { text: qsTr("Settings") }
            MenuItem {
                text: qsTr("Reset all to default")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("All settings cleared"), function() { app.resetSettings() } )}
            }
        }
        PushUpMenu {
            visible: (app.pignore.count > 0 || app.ignore.count > 0)
            MenuItem {
                enabled: pList.count > 0
                text: qsTr("Clear permanent ignore list")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("List Cleared."), function() { app.resetIgnorePerm() } )}
            }
            MenuItem {
                enabled: iList.count > 0
                text: qsTr("Clear session ignore list")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("List Cleared."), function() { app.ignore.clear(); } )}
            }
        }
        */
        VerticalScrollDecorator {}
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
