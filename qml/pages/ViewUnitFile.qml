/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Share 1.0

Page {
    id: page

    property string unitName: ""
    property string fileName: ""
    property string fileData: ""

    onStatusChanged: (status == DialogStatus.Opened) ? loadFile() : 0

    function loadFile() {
        var fileUrl = "file://" + page.fileName

        var r = new XMLHttpRequest()
        r.open('GET', fileUrl);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.send();

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                page.fileData  = r.response;
            }
        }
    }

    Loader { id:shareLoader
        active: app.osVersionI > 41000
        ShareAction { id: share; mimeType: "text/*" }
    }

    PullDownMenu {
        flickable: view
        MenuItem { text: qsTr("Share %1…").arg(qsTr("Unit File", "argument for 'Share'"))
            enabled: ( (page.fileName.length > 0) && (page.fileData.length > 0) )
            onClicked: {
                var r = [
                    page.fileName,
                    { "data": page.fileData, "name": page.unitName }
                ];
                share.resources = r;
                share.mimeType = "text/x-systemd-unit";
                share.trigger();
            }
        }
        MenuItem { text: qsTr("Share as Text…")
            enabled: ( (page.fileName.length > 0) && (page.fileData.length > 0) )
            onClicked: {
                var r = [
                    page.fileName,
                    { "data": page.fileData, "name": page.unitName }
                ];
                share.mimeType = "text/plain";
                share.resources = r;
                share.trigger();
            }
        }
        MenuItem { text: qsTr("Open…")
            enabled: ( (page.fileName.length > 0) && (page.fileData.length > 0) )
            onClicked: {
                var fileUrl = "file://" + page.fileName
                Qt.openUrlExternally(fileUrl);
            }
        }
        MenuItem { text: qsTr("Copy %1 to clipboard").arg(qsTr("file path", "argument for 'copy to clipboard' menu entry")); enabled: page.fileName.length > 0; onClicked: { Clipboard.text = page.fileName; app.popup(qsTr("%1 copied to clipboard").arg("file name", "argument for 'copied to clipboard' popup message"),page.fileName)} }
        MenuItem { text: qsTr("Copy %1 to clipboard").arg(qsTr("file contents", "argument for 'copy to clipboard' menu entry"));  enabled: page.fileData.length > 0; onClicked: { Clipboard.text = page.fileData; app.popup(qsTr("%1 copied to clipboard").arg("file contents", "argument for 'copied to clipboard' popup message"),"")} }
    }

    SilicaListView {
        id: view
        header: PageHeader { title: unitName; description: fileName; width: view.width
            Separator { anchors.verticalCenter: parent.bottom; width: parent.width; color: Theme.primaryColor }
        }
        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - Theme.horizontalPageMargin
        topMargin: Theme.paddingLarge
        bottomMargin: Theme.paddingLarge
        model: 1
        delegate: SilicaFlickable {
            width: ListView.view.width
            height: ListView.view.height
            contentHeight: content.height
            contentWidth: content.width
            Label { id: content
                //x: Theme.horizontalPageMargin
                anchors.topMargin: Theme.paddingLarge
                anchors.bottomMargin: Theme.paddingLarge
                anchors.leftMargin: Theme.paddingMedium
                anchors.rightMargin: Theme.paddingMedium

                text: page.fileData.length > 0 ? page.fileData : ""

                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                font.family: "monospace"
                wrapMode: Text.NoWrap
                verticalAlignment: Text.AlignVCenter
            }
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
