/*
 * This file is part of SailorD.
 * Copyright (c) 2022,2023 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    onStatusChanged: if (coverPage.status === Cover.Active) { update() }

    Component.onCompleted:  {
        app.refreshed.connect(update);
        update();
    }

    function update() {
        const di = app.nfailed - app.ignore.count;
        ignoredCounter.text = (di > 0) ? Number(di) : "0";
        failedCounter.text = app.nfailed;
    }

    Row { id: row
        x: Theme.horizontalPageMargin
        y: Theme.horizontalPageMargin
        spacing: Theme.paddingSmall
        Label { id: ignoredCounter
            text: "?"
            font.pixelSize: Theme.fontSizeHuge
            anchors.verticalCenter: col.verticalCenter
        }
        Label {
            text: "/"
            font.pixelSize: Theme.fontSizeLarge
            anchors.bottom: col.bottom
        }
        Label { id: failedCounter
            text: "?"
            font.pixelSize: Theme.fontSizeLarge
            anchors.bottom: col.bottom
        }
        Item {
            height: row.height
            width: row.spacing
        }
        Column { id: col
            Label { font.pixelSize: Theme.fontSizeExtraSmall; text: qsTr("failed") }
            Label { font.pixelSize: Theme.fontSizeExtraSmall; text: qsTr("units") }
        }
    }
    /*
    ColumnView { id: failedList
        x: Theme.horizontalPageMargin
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.height/3
        maximumVisibleHeight: parent.height/3
        itemHeight: Theme.itemSizeSmall
        delegate: Component { Label {
            text: unit
            font.pixelSize: Theme.fontSizeTiny
        }}
    }
    */

    CoverPlaceholder {
        text: "<b>[</b> ● ◀ <b>]</b>" + "\n" + Qt.application.name
        textColor: Theme.highlightColor
        CoverActionList {
            CoverAction { iconSource: "image://theme/icon-cover-sync"; onTriggered: {app.refresh(); update()} }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
